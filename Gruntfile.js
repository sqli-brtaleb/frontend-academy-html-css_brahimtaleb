module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'dist/styles.min.css': 'src/styles/styles.scss'
                }
            }
        },
        cssmin:{
            my_target:{
                files: [{
                    expand: true,
                    src: 'dist/styles.min.css',
                    dest: '',
                    ext: '.min.css'
                }]
            }
        },
        watch: {
            css: {
                files: ['src/styles/*.scss'],
                tasks: ['sass','cssmin']
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['sass','cssmin']);

};